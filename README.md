# Online-shop (FE)
Build online-shop using MERN stack

## Demo
[Link](https://onlineshop123-react.web.app/)

## Requirements
- Install Node

## Using
- react
- react-redux
- redux-thunk
- react-router-dom
- react-strap

## Features
- Add, Edit, Remove Product
- Add to Cart, Remove Item From Cart
- Place Order
- Login, Register User

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
> npm start


## BackEnd
[Link](https://gitlab.com/online-shop1/onlineshop-api)
