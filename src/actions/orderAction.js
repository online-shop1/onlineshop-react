import * as types from '../constants'

export const fetchOrders = (data)=>{
    return {
        type:types.FETCH_ORDERS,
        payload: data
    }
}

export const createOrder = (data)=>{
    return {
        type: types.PLACE_ORDER,
        payload: data
    }
}

export const fetchOrderDetails = (data)=>{
    return{
        type: types.FETCH_ORDER_DETAILS,
        payload: data
    }
}