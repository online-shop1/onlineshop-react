import * as types from '../constants'


export const loginUser =(user)=>{
    return {
        type: types.USER_LOGIN,
        payload: user
    }
}
export const logoutUser =()=>{
    return {
        type: types.USER_LOGOUT
    }
}

export const registerUser =(user)=>{
    return {
        type: types.USER_REGISTER,
        payload: user
    }
}