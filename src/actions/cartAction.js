import * as types from '../constants'


export const addToCart =(product, qty)=>{
    return {
        type: types.ADD_TO_CART,
        payload: {product, qty}
    }
}
export const deleteItemInCart = (id)=>{
    return {
        type: types.DELETE_ITEM_IN_CART,
        payload: id
    }
}
export const removeCart = ()=>{
    return {
        type: types.REMOVE_ALL_ITEMS
    }
}

export const saveShipping = (data)=>{
    return {
        type: types.SAVE_SHIPPING,
        payload: data
    }
}
export const savePayment = (data)=>{
    return {
        type: types.SAVE_PAYMENT,
        payload: data
    }
}