import * as types from '../constants'

// fetch productList
export const fetchProductsRequest = ()=>{
    return {
        type : types.FETCH_PRODUCTS_REQUEST
    }
}
export const fetchProductsSuccess = (data)=>{
    return {
        type : types.FETCH_PRODUCTS_SUCCESS,
        payload: data
    }
}
export const fetchProductsFailure = (error)=>{
    return {
        type : types.FETCH_PRODUCTS_FAILURE,
        payload: error
    }
}

// fetch detailProduct
export const fetchDetailProductRequest =()=>{
    return {
        type: types.FETCH_DETAIL_PRODUCT_REQUEST
    }
}
export const fetchDetailProductSuccess =(product)=>{
    return {
        type: types.FETCH_DETAIL_PRODUCT_SUCCESS,
        payload: product
    }
}
export const fetchDetailProductFailure =(error)=>{
    return {
        type: types.FETCH_DETAIL_PRODUCT_FAILURE,
        payload: error
    }
}

//
export const saveProduct = (product)=>{
    return {
        type: types.SAVE_PRODUCT,
        payload: product
    }
}

export const deleteProduct = (product)=>{
    return {
        type: types.DELETE_PRODUCT,
        payload: product
    }
}