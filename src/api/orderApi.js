import {default as axiosClient} from './axiosClient'

const orderApi = {
    getAll: ()=>{
        const url = '/orders';
        return axiosClient.get(url)
    },
    getOne :(id)=>{
        const url = `/orders/${id}`
        return axiosClient.get(url)
    },
    create:(data)=>{
        const url = `/orders`;
        return axiosClient.post(url, data)
    }
}

export default orderApi