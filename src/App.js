import React from 'react';

import { Container } from 'react-bootstrap'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import './App.scss';
import Header from './components/Header/Header';
import HomeScreen from './Screens/HomeScreen';
import ProductScreen from './Screens/ProductScreen';
import CartScreen from './Screens/CartScreen';
import RegisterScreen from './Screens/RegisterScreen';
import LoginScreen from './Screens/LoginScreen';
import ProductsManagerScreen from './Screens/ProductsManagerScreen';
import ProductManagerScreen from './Screens/ProductManagerScreen';
import ShippingScreen from './Screens/ShippingScreen';
import PaymentScreen from './Screens/PaymentScreen';
import PlaceOrderScreen from './Screens/PlaceOrderScreen';
import OrdersManagerScreen from './Screens/OrdersManagerScreen';
import OrderDetailScreen from './Screens/OrderDetailScreen';


function App() {
  return (
    <BrowserRouter>
      <div className="App grid-container">
        <Header className='header' />
        <div className='main'>
          <Container>
            <Switch>
              <Route path='/' exact component={HomeScreen} />
              <Route path='/product/:id' component={ProductScreen} />
              <Route path='/cart' component={CartScreen} />
              <Route path='/register' component={RegisterScreen} />
              <Route path='/login' component={LoginScreen} />
              <Route path='/admin/products' exact component={ProductsManagerScreen} />
              <Route path='/admin/products/create' exact component={ProductManagerScreen} />
              <Route path='/admin/products/edit/:id' exact component={ProductManagerScreen} />
              <Route path='/shipping'  component={ShippingScreen} />
              <Route path='/payment' exact component={PaymentScreen} />
              <Route path='/placeorder' exact component={PlaceOrderScreen} />
              <Route path='/admin/orders' exact component={OrdersManagerScreen} />
              <Route path='/admin/orders/:id' exact component={OrderDetailScreen} />
              <Route ><div>Page not Found!</div></Route>
            </Switch>
          </Container>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
