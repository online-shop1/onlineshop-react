export const products = [
    {
        _id: "1",
        name: "I Phone 6",
        image: "/images/ip6.jpg",
        price: 60,
        brand: "Apple",
        rating: 5,
        numReviews: 10,
        countInStock: 5
    },
    {
        _id: "2",
        name: "I Phone X",
        image: "/images/ipx.jpeg",
        price: 640,
        brand: "Apple",
        rating: 5,
        numReviews: 20,
        countInStock: 4
    },
    {
        _id: "3",
        name: "SamSung Note 10",
        image: "/images/note10.jpeg",
        price: 340,
        brand: "SamSung",
        rating: 4.5,
        numReviews: 10,
        countInStock: 10
    },
    {
        _id: "4",
        name: "I Pad 4",
        image: "/images/ipad4.jpeg",
        price: 60,
        brand: "Apple",
        rating: 5,
        numReviews: 15,
        countInStock: 7
    }, {
        _id: "5",
        name: "Macbook Air 2017",
        image: "/images/mac2017.jpeg",
        price: 660,
        brand: "Apple",
        rating: 4,
        numReviews: 3,
        countInStock: 3
    }, {
        _id: "6",
        name: "SamSung Galaxy 10",
        image: "/images/galaxy10.jpeg",
        price: 790,
        brand: "SamSung",
        rating: 5,
        numReviews: 19,
        countInStock: 4
    }
]
export const detailProduct = {
    _id: "2",
    name: "I Phone X",
    image: "/images/ipx.jpeg",
    price: 640,
    brand: "Apple",
    rating: 5,
    numReviews: 20,
    countInStock: 4
}