import React from 'react'
import { Link,useHistory } from 'react-router-dom'

import { Card, Button } from 'react-bootstrap'
import {useDispatch} from 'react-redux'
import { addToCart } from '../../actions/cartAction';

function ProductItem(props) {
    const { product } = props;
    const dispatch = useDispatch()
    const history = useHistory()
    const onAddCart = (product, qty)=>{
        dispatch(addToCart(product, qty))
        history.push('/cart')
    }
    return (
        <Card className="text-center" >
            <Link to={`/product/${product._id}`}><Card.Img variant="top" src={product.image} style={{ height: '18rem', width: '18rem', margin: '0 auto' }} /></Link>
            <Card.Body className='cards' style={{ fontSize: '1.5rem' }} >
                <Card.Title ><Link to={`/product/${product._id}`}>{product.name}</Link></Card.Title>
                <Card.Subtitle className="mb-2 text-dark">{product.rating} star ({product.numReviews} reviews)</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">{product.brand}</Card.Subtitle>
                <Card.Text><strong>$ {product.price}</strong></Card.Text>
                {product.countInStock > 0 ?
                 <Button variant="primary" style={{ fontSize: '1.5rem' }} onClick={()=>onAddCart(product, 1)} >Add to Cart</Button> :
                 <Button disabled style={{ fontSize: '1.5rem' }}>Out of Stock</Button>
                 }
            </Card.Body>
        </Card>
    )
}

export default ProductItem
