import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { logoutUser } from '../../actions/userAction'

function Header() {
    const history = useHistory()
    const dispatch = useDispatch()
    const userSignin = useSelector(state => state.userInfo)
    const { user } = userSignin;

    const cart = useSelector(state => state.cart)
    const { cartItems } = cart;

    const onLogOut = () => {
        dispatch(logoutUser())
        history.push('/login')
    }

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand as='div' style={{ fontSize: '2rem', color: '#212529' }}><Link to='/'>Online Shop</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto" style={{ fontSize: '1.5rem' }}>
                        <Nav.Link as='div'><Link to='/'>Home</Link></Nav.Link>
                        {/* <NavDropdown title="Products" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Apple</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">Samsung</NavDropdown.Item>
                        </NavDropdown> */}
                        <Nav.Link as='div'><Link to='/cart'>Cart{cartItems.length > 0 && `(${cartItems.reduce((a, b) => a + Number(b.qty), 0)})`}</Link></Nav.Link>
                        {user && user.user.isAdmin &&
                            <NavDropdown title="Admin" id="basic-nav-dropdown" >
                                <Link to='/admin/products'><NavDropdown.Item as='div'>Products</NavDropdown.Item></Link>
                                <Link to='/admin/orders'><NavDropdown.Item as='div'>Orders</NavDropdown.Item></Link>
                            </NavDropdown>
                        }
                        {!user ?
                            <Nav.Link as='div'><Link to='/login'>Sign In</Link></Nav.Link> :
                            <Nav.Link as='div' style={{ cursor: 'pointer' }} onClick={onLogOut}>Sign Out</Nav.Link>
                        }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default Header
