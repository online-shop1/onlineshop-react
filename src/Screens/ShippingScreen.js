import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

import {  useHistory } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import { saveShipping } from '../actions/cartAction';
import CheckoutStep from '../components/CheckoutStep';

function ShippingScreen() {
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [country, setCountry] = useState('');
    const dispatch = useDispatch();
    const history = useHistory()

    const onSubmit = (e) => {
        e.preventDefault();
        dispatch(saveShipping({ address, city, postalCode, country }));
        history.push('/payment');
    }
    return (
        <div>
            <CheckoutStep step1 step2></CheckoutStep>
            <div className='form'>
                <Form className='form__container' onSubmit={(e) => onSubmit(e)}>
                    <h3 className='form__header'>Shipping</h3>
                    <Form.Group controlId="formBasicAddress">
                        <Form.Label>Address</Form.Label>
                        <Form.Control type="text" placeholder="Enter Address"
                            value={address} name='address'
                            onChange={(e) => setAddress(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control type="text" placeholder="Enter City"
                            value={city} name='city'
                            onChange={(e) => setCity(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicPostalCode">
                        <Form.Label>PostalCode</Form.Label>
                        <Form.Control type="text" placeholder="PostalCode"
                            value={postalCode} name='postalCode'
                            onChange={(e) => setPostalCode(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicCountry">
                        <Form.Label>Country</Form.Label>
                        <Form.Control type="text" placeholder="Country"
                            value={country} name='country'
                            onChange={(e) => setCountry(e.target.value)}
                        />
                    </Form.Group>
                    <Button className='btnn btnn--primary btnn--full' type="submit">
                        Continue
                </Button>
                </Form>
            </div>
        </div>
    )
}

export default ShippingScreen
