import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap'
import CheckoutStep from '../components/CheckoutStep';
import { savePayment } from '../actions/cartAction';

function PaymentScreen() {
    const [paymentMethod, setPaymentMethod] = useState('')
    const dispatch = useDispatch();
    const history = useHistory()

    const onSubmit = (e) => {
        e.preventDefault(); 
        dispatch(savePayment({ paymentMethod }));
        history.push('/placeorder');
    }
    return (
        <div>
            <CheckoutStep step1 step2 step3></CheckoutStep>
            <div className='form'>
                <Form className='form__container' onSubmit={(e) => onSubmit(e)}>
                    <h3 className='form__header'>Payment</h3>
                    <Form.Check
                        type="radio"
                        label="Paypal"
                        name="paymentMethod"
                        value='paypal'
                        id="formHorizontalPaymentMetod"
                        style={{display:'flex', alignItems:'center'}}
                        onChange={(e)=>setPaymentMethod(e.target.value)}
                    />
                    <Form.Check
                        type="radio"
                        label="2Checkout"
                        name="paymentMethod"
                        value='2checkout'
                        id="formHorizontalPaymentMetod2"
                        style={{display:'flex', alignItems:'center'}}
                        onChange={(e)=>setPaymentMethod(e.target.value)}
                    />

                    <Button className='btnn btnn--primary btnn--full' type="submit">
                        Continue
                </Button>
                </Form>
            </div>
        </div>
    )
}

export default PaymentScreen
