import React, { useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

import ProductItem from "../components/Products/ProductItem";
import {
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductsFailure,
} from "../actions/productAction";
import productApi from "../api/productApi";

function HomeScreen() {
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.productList);

  const { products, loading, error } = productList;
  useEffect(() => {
    const fetchProducts = async () => {
      try {
        dispatch(fetchProductsRequest());
        const response = await productApi.getAll();
        dispatch(fetchProductsSuccess(response));
      } catch (error) {
        dispatch(fetchProductsFailure(error.message));
      }
    };
    fetchProducts();
    return () => {
      //
    };
  }, []);

  return (
    <div>
      {loading ? (
        <div>Loading....</div>
      ) : error ? (
        <h3>{error}</h3>
      ) : (
        <Row className="my-5">
          {products &&
            products.map((product, index) => {
              return (
                <Col md={4} className="my-3" key={index}>
                  <ProductItem product={product} />
                </Col>
              );
            })}
        </Row>
      )}
    </div>
  );
}

export default HomeScreen;
