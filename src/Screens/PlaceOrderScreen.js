import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import CheckoutStep from '../components/CheckoutStep';
import { removeCart } from '../actions/cartAction';
import orderApi from '../api/orderApi';
import { createOrder } from '../actions/orderAction';

function PlaceOrderScreen() {
    const cart = useSelector(state => state.cart);
    const userSignin = useSelector(state => state.userInfo);
    const {user} = userSignin.user
    
    const { cartItems, shipping, payment } = cart;

    const itemsPrice = cartItems.reduce((a, c) => a + c.product.price * c.qty, 0);
    const shippingPrice = itemsPrice > 100 ? 0 : 10;
    const taxPrice = 0.15 * itemsPrice;
    const totalPrice = itemsPrice + shippingPrice + taxPrice;
    const dispatch = useDispatch();
    const history = useHistory()

    if (!shipping.address) {
        history.push("/shipping");
    } else if (!payment.paymentMethod) {
        history.push("/payment");
    }
    const placeOrderHandler = () => {
        const createOrderRequest = async (data)=>{
            try {
                const response =  await orderApi.create(data)
                dispatch(createOrder(response.data));
                dispatch(removeCart())
                history.push('/')
            } catch (error) {
                console.log("Error In create Order.", error)
            }
        }
        createOrderRequest({
                orderItems: cartItems, shipping, payment, itemsPrice, shippingPrice,
                taxPrice, totalPrice, userId : user._id
            })
       
    }

    return (
        <div>
            <CheckoutStep step1 step2 step3 step4 ></CheckoutStep>
            <div className="placeorder">
                <div className="placeorder-info">
                    <div>
                        <h3>
                            Shipping
                        </h3>
                        <div>
                            {shipping.address}, {shipping.city},
                            {shipping.postalCode}, {shipping.country},
                        </div>
                    </div>
                    <div>
                        <h3>Payment</h3>
                        <div>
                            Payment Method: {cart.payment.paymentMethod}
                        </div>
                    </div>
                    <div>
                        <div className='cart__list'>
                            <ul className='cart__list__container'>
                                <li>
                                    <h3>Shopping Cart</h3>
                                    <div>Price</div>
                                </li>
                                {!cartItems.length ?
                                    <div style={{ textAlign: 'center' }}>Cart is empty</div> :
                                    cartItems.map((item, index) => {
                                        return (
                                            <li key={index}>
                                                <div className='cart__list__image'>
                                                    <img src={item.product.image} alt='asd' />
                                                </div>
                                                <div className='cart__list__name'>
                                                    <Link to={`/products/${item.product._id}`}>
                                                        <div>{item.product.name}</div>
                                                    </Link>
                                                    <div>Qty: {item.qty}

                                                    </div>
                                                </div>
                                                <div className='cart__list__price'>$ {item.product.price}</div>
                                            </li>
                                        )
                                    })
                                }

                            </ul>
                        </div>
                    </div>
                </div>
                <div className="placeorder-action">
                    <ul>
                        <li>
                            <button className="btnn btnn--primary btnn--full" onClick={() => placeOrderHandler()} >Place Order</button>
                        </li>
                        <li>
                            <h3>Order Summary</h3>
                        </li>
                        <li>
                            <div>Items</div>
                            <div>${itemsPrice}</div>
                        </li>
                        <li>
                            <div>Shipping</div>
                            <div>${shippingPrice}</div>
                        </li>
                        <li>
                            <div>Tax</div>
                            <div>${taxPrice}</div>
                        </li>
                        <li>
                            <div>Order Total</div>
                            <div>${totalPrice}</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default PlaceOrderScreen
