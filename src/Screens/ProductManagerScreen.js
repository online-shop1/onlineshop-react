import React, { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import { saveProduct, fetchDetailProductRequest, fetchDetailProductSuccess, fetchDetailProductFailure } from '../actions/productAction'
import { useDispatch, useSelector } from 'react-redux'
import productApi from '../api/productApi'
import { Link, useHistory, useRouteMatch } from 'react-router-dom'

function ProductManagerScreen() {
    const [id, setId] = useState('')
    const [name, setName] = useState('')
    const [brand, setBrand] = useState('')
    const [image, setImage] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [category, setCategory] = useState('')
    const [countInStock, setCountInStock] = useState('')
    const dispatch = useDispatch()
    const history = useHistory()
    const router = useRouteMatch()
    const productDetails = useSelector(state => state.productDetail)
    const { product } = productDetails

    const productId = router.params.id
    useEffect(() => {
        if (productId) {
            const fetchProduct = async () => {
                try {
                    dispatch(fetchDetailProductRequest());
                    const response = await productApi.getOne(productId);
                    dispatch(fetchDetailProductSuccess(response));
                } catch (error) {
                    dispatch(fetchDetailProductFailure(error.message));
                }
            }
            fetchProduct()
        }
        return () => {
            //
        }
    }, [router])

    useEffect(() => {
        setId(product._id)
        setName(product.name)
        setPrice(product.price)
        setImage(product.image)
        setBrand(product.brand)
        setCategory(product.category)
        setDescription(product.description)
        setCountInStock(product.countInStock)
    }, [product,router])

    const onSubmit = (e) => {
        e.preventDefault()
        if (id) {
            const updateProduct = async(data)=>{
                try {
                    const product = await productApi.update(id, data)
                    dispatch(saveProduct(product.data))
                    history.goBack()
                } catch (error) {
                    console.log("Error in Updating Product.", error)
                }
            }
            updateProduct({ name, price, image, brand, category, countInStock, description })
        } else {
            const createProduct = async (data) => {
                try {
                    const product = await productApi.create(data)
                    dispatch(saveProduct(product.data))
                    history.goBack()
                } catch (error) {
                    console.log("Error In create Product.", error)
                }

            }
            createProduct({ name, price, image, brand, category, countInStock, description })
        }
    }

    return (
        <div>
            <Link to="/admin/products">Go back products</Link>
            <div className='form' >
                <Form className='form__container' onSubmit={(e) => onSubmit(e)}>
                    <h3 className='form__header'>Create Product</h3>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name"
                            value={name} name='name'
                            onChange={(e) => setName(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicBrand">
                        <Form.Label>Brand</Form.Label>
                        <Form.Control type="text" placeholder="Brand"
                            value={brand} name='brand'
                            onChange={(e) => setBrand(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicCategory">
                        <Form.Label>Category</Form.Label>
                        <Form.Control type="text" placeholder="Category"
                            value={category} name='category'
                            onChange={(e) => setCategory(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="text" placeholder="Price"
                            value={price} name='price'
                            onChange={(e) => setPrice(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicImage">
                        <Form.Label>Image</Form.Label>
                        <Form.Control type="text" placeholder="Link Image"
                            value={image} name='image'
                            onChange={(e) => setImage(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Description"
                            value={description} name='description'
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicCountInStock">
                        <Form.Label>CountInStock</Form.Label>
                        <Form.Control type="text" placeholder="CountInStock"
                            value={countInStock} name='countInStock'
                            onChange={(e) => setCountInStock(e.target.value)}
                        />
                    </Form.Group>
                    <Button className='btnn btnn--primary btnn--full' type="submit">
                        Create
                </Button>
                </Form>
            </div>
        </div>
    )
}

export default ProductManagerScreen
