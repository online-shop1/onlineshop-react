import React, { useEffect, useState } from "react";
import { Link, useRouteMatch, useHistory } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import {
  fetchDetailProductSuccess,
  fetchDetailProductFailure,
  fetchDetailProductRequest,
} from "../actions/productAction";
import { addToCart } from "../actions/cartAction";
import productApi from "../api/productApi";
function ProductScreen() {
  const router = useRouteMatch();
  const { id } = router.params;
  const productDetails = useSelector((state) => state.productDetail);
  const { product, loading, error } = productDetails;
  const dispatch = useDispatch();
  const history = useHistory();
  const [qty, setQty] = useState(1);
  useEffect(() => {
    const fetchProduct = async () => {
      try {
        dispatch(fetchDetailProductRequest());
        const response = await productApi.getOne(id);
        dispatch(fetchDetailProductSuccess(response));
      } catch (error) {
        dispatch(fetchDetailProductFailure(error.message));
      }
    };
    fetchProduct();
    return () => {
      //
    };
  }, []);
  const onAddToCart = (product, qty) => {
    dispatch(addToCart(product, qty));
    history.push("/cart");
  };
  return (
    <div>
      {loading ? (
        <div>Loading....</div>
      ) : error ? (
        <h3>{error}</h3>
      ) : (
        <div>
          <div className="back-to-result">
            <Link to="/" style={{ fontSize: "1.5rem", color: "#ff8000" }}>
              Back to result
            </Link>
          </div>
          <div className="details ">
            <div className="details__image">
              <img src={product.image} alt="..." />
            </div>
            <div className="details__info">
              <ul>
                <li>
                  <h4>{product.name} </h4>
                </li>
                <li>
                  {product.rating} stars ({product.numReviews} reviews)
                </li>
                <li>
                  Price: <strong>$ {product.price} </strong>
                </li>
                <li>
                  Description:
                  <span>{product.description}</span>
                </li>
              </ul>
            </div>
            <div className="details__action">
              <ul>
                <li>
                  <strong>Price: ${qty * product.price}</strong>
                </li>
                <li>
                  Status:{" "}
                  {product.countInStock > 0 ? "In Stock" : "Unavailable"}
                </li>
                <li>
                  Qty:
                  <select value={qty} onChange={(e) => setQty(e.target.value)}>
                    {[...Array(product.countInStock).keys()].map((x) => (
                      <option key={x + 1} value={x + 1}>
                        {x + 1}
                      </option>
                    ))}
                  </select>
                </li>
                <li>
                  {product.countInStock > 0 && (
                    <button
                      className="btnn btnn--primary"
                      onClick={() => onAddToCart(product, qty)}
                    >
                      Add to Cart
                    </button>
                  )}
                </li>
              </ul>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default ProductScreen;
