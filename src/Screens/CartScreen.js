import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { addToCart, deleteItemInCart } from '../actions/cartAction'
function CartScreen() {
    const cart = useSelector(state => state.cart)
    const dispatch = useDispatch()
    const { cartItems } = cart;
    const history = useHistory()
    const onDeleteItem = (id) => {
        dispatch(deleteItemInCart(id))
    }

    const checkoutHandler = () => {
        history.push("/login?redirect=shipping");
    }
    return (
        <div className='cart'>
            <div className='cart__list'>
                <ul className='cart__list__container'>
                    <li>
                        <h3>Shopping Cart</h3>
                        <div>Price</div>
                    </li>
                    {!cartItems.length ?
                        <div style={{ textAlign: 'center' }}>Cart is empty</div> :
                        cartItems.map((item, index) => {
                            return (
                                <li key={index}>
                                    <div className='cart__list__image'>
                                        <img src={item.product.image} alt='asd' />
                                    </div>
                                    <div className='cart__list__name'>
                                        <Link to={`/product/${item.product._id}`}>
                                            <div>{item.product.name}</div>
                                        </Link>
                                        <div>Qty:
                                            <select value={item.qty} onChange={(e) => dispatch(addToCart(item.product, e.target.value))} >
                                                {[...Array(item.product.countInStock).keys()].map(x =>
                                                    <option key={x + 1} value={x + 1}>{x + 1}</option>
                                                )}
                                            </select>
                                            <button type='button' className='btnn' onClick={() => onDeleteItem(item.product._id)} >remove</button>
                                        </div>
                                    </div>
                                    <div className='cart__list__price'>$ {item.product.price}</div>
                                </li>
                            )
                        })
                    }

                </ul>
            </div>
            <div className='cart__action'>
                <h3>
                    Subtotal ({cartItems.reduce((a, b) => a + Number(b.qty), 0)} items)
                    : $ {cartItems.reduce((a, b) => a + Number(b.qty) * Number(b.product.price), 0)}
                </h3>
                {cartItems.length > 0 &&
                    <button onClick={checkoutHandler} className='btnn btnn--primary btnn--full' >
                        Proceed Checkout
                    </button>
                }
            </div>
        </div>
    )
}

export default CartScreen
