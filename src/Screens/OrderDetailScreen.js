import React, { useEffect } from 'react'
import orderApi from '../api/orderApi';
import { useRouteMatch, Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchOrderDetails } from '../actions/orderAction';

function OrderDetailScreen() {
    const router = useRouteMatch()
    const { id } = router.params;
    const orderDetail = useSelector(state => state.orderDetail);
    const { order } = orderDetail;
    const dispatch = useDispatch()

    useEffect(() => {
        const fetchOrder = async (id) => {
            try {
                const response = await orderApi.getOne(id)
                dispatch(fetchOrderDetails(response))
            } catch (error) {
                console.log('Failed to fetch Order, ', error)
            }

        }
        fetchOrder(id)
        return () => {
        }
    }, [])
    return (
        <div>
            { !(Object.keys(order).length === 0) &&
                <div className="placeorder">
                    <div className="placeorder-info">
                        <div>
                            <h3>
                                Shipping
                    </h3>
                            <div>
                                {order.shipping.address}, {order.shipping.city},
                        {order.shipping.postalCode}, {order.shipping.country},
                    </div>
                        </div>
                        <div>
                            <h3>Payment</h3>
                            <div>
                                Payment Method: {order.payment.paymentMethod}
                            </div>
                        </div>
                        <div>
                            <div className='cart__list'>
                                <ul className='cart__list__container'>
                                    <li>
                                        <h3>Shopping Cart</h3>
                                        <div>Price</div>
                                    </li>
                                    {!order.orderItems.length ?
                                        <div style={{ textAlign: 'center' }}>Cart is empty</div> :
                                        order.orderItems.map((item, index) => {
                                            return (
                                                <li key={index}>
                                                    <div className='cart__list__image'>
                                                        <img src={item.product.image} alt='asd' />
                                                    </div>
                                                    <div className='cart__list__name'>
                                                        <Link to={`/products/${item.product._id}`}>
                                                            <div>{item.product.name}</div>
                                                        </Link>
                                                        <div>Qty: {item.qty}

                                                        </div>
                                                    </div>
                                                    <div className='cart__list__price'>$ {item.product.price}</div>
                                                </li>
                                            )
                                        })
                                    }

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="placeorder-action">
                        <ul>
                            <li>
                                <h3>Order Summary</h3>
                            </li>
                            <li>
                                <div>Items</div>
                                <div>${order.itemsPrice}</div>
                            </li>
                            <li>
                                <div>Shipping</div>
                                <div>${order.shippingPrice}</div>
                            </li>
                            <li>
                                <div>Tax</div>
                                <div>${order.taxPrice}</div>
                            </li>
                            <li>
                                <div>Order Total</div>
                                <div>${order.totalPrice}</div>
                            </li>
                        </ul>
                    </div>
                </div> 
            }
        </div>
    )
}

export default OrderDetailScreen
