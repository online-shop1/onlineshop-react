import React, { useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import productApi from "../api/productApi";
import {
  deleteProduct,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductsFailure,
} from "../actions/productAction";

function ProductsManagerScreen() {
  const productList = useSelector((state) => state.productList);
  const productSave = useSelector((state) => state.productSave);
  const userSignin = useSelector((state) => state.userInfo);
  const { user } = userSignin;

  const { product: saveProduct } = productSave;
  const { products, error, loading } = productList;

  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    if (!user || (user && !user.user.isAdmin)) {
      history.push("/");
    }
    return () => {};
  }, []);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        dispatch(fetchProductsRequest());
        const response = await productApi.getAll();
        dispatch(fetchProductsSuccess(response));
        // dispatch(productlist(response))
      } catch (error) {
        dispatch(fetchProductsFailure(error.message));
        console.log("Failed to fetch products, ", error);
      }
    };
    fetchProducts();
    return () => {
      //
    };
  }, [saveProduct]);

  const onDelete = async (product) => {
    try {
      await productApi.delete(product._id);
      dispatch(deleteProduct(product));
    } catch (error) {
      console.log("Failed to delete product, ", error);
    }
  };
  return (
    <div>
      {loading ? (
        <div>Loading....</div>
      ) : error ? (
        <h3>{error}</h3>
      ) : (
        <div className="products-table">
          <div className="products-table__heading">
            <h3>Products Management</h3>
            <Link to="/admin/products/create">
              <Button variant="success">Create product</Button>
            </Link>
          </div>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Brand</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {!products.length ? (
                <tr>
                  <td colSpan="7">Loading...</td>
                </tr>
              ) : (
                products.map((product, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                    <td>{product.brand}</td>
                    <td>
                      <Link to={`/admin/products/edit/${product._id}`}>
                        <Button variant="warning" className="mr-2">
                          Edit
                        </Button>
                      </Link>
                      <Button
                        variant="danger"
                        onClick={() => {
                          onDelete(product);
                        }}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </Table>
        </div>
      )}
    </div>
  );
}

export default ProductsManagerScreen;
