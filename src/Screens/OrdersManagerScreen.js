import React, { useEffect } from 'react'
import { Table, Button } from 'react-bootstrap'
import orderApi from '../api/orderApi'
import { useDispatch, useSelector } from 'react-redux'
import { fetchOrders } from '../actions/orderAction'
import { Link, useHistory } from 'react-router-dom'

function OrdersManagerScreen() {
    const orderList = useSelector(state => state.orders)
    const userSignin = useSelector(state => state.userInfo)
    const { user } = userSignin;
    const { orders } = orderList
    const dispatch = useDispatch()
    const history = useHistory()

    useEffect(() => {
        if(!user || (user && !user.user.isAdmin)){
            history.push('/')
        }
        return () => {
        }
    }, [history])

    useEffect(() => {
        const fetchOrderList = async () => {
            const response = await orderApi.getAll()
            dispatch(fetchOrders(response))
        }
        fetchOrderList()
        return () => {
            //
        }
    }, [])

    return (
        <div className='products-table'>
            <div className="products-table__heading">
                <h3>Orders Management</h3>
            </div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Total ($)</th>
                        <th>User</th>
                        <th>PAID</th>
                        <th>PAID AT</th>
                        <th>DELIVERED</th>
                        <th>DELIVERED AT</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    {!orders.length ? <tr><td colSpan="10">Orders List is Empty</td></tr> :
                        orders.map((order, index) =>
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{order._id}</td>
                                <td>{order.createdAt}</td>
                                <td>{order.totalPrice}</td>
                                <td>{order.user.fullname}</td>
                                <td>{order.isPaid.toString()}</td>
                                <td>{order.paidAt}</td>
                                <td>{order.isDelivered.toString()}</td>
                                <td>{order.deliveredAt}</td>
                                <td><Link to={`/admin/orders/${order._id}`}><Button>Detail</Button></Link></td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>
        </div>
    )
}

export default OrdersManagerScreen
