import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import authApi from '../api/authApi'
import { registerUser } from '../actions/userAction'

function RegisterScreen() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [fullname, setFullname] = useState('')

    const dispatch = useDispatch();
    const history = useHistory()
    const location = useLocation()

    const userSignin = useSelector(state => state.userInfo)
    const { user } = userSignin
    console.log(user);
    
    const redirect = location.search ? location.search.split('=')[1] : '/'

    useEffect(() => {
        if (user) {
            history.push(redirect)
        }
        return () => {
            //
        }
    }, [user])
    const onSubmit = (e) => {
        e.preventDefault();
        const register = async (data) => {
            try {
                const response = await authApi.register(data);
                dispatch(registerUser(response))
            } catch (error) {
                console.log("Failed to Register :", error)
            }
        }
        register({ email, fullname, password })
    }

    return (
        <div className='form'>
            <Form className='form__container' onSubmit={(e) => onSubmit(e)}>
                <h3 className='form__header'>Register</h3>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email"
                        value={email} name='email'
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicFullName">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter Full Name"
                        value={fullname} name='fullname'
                        onChange={(e) => setFullname(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                        value={password} name='password'
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <Button className='btnn btnn--primary btnn--full' type="submit">
                    Register
                </Button>
                <div className='form__link'>
                    <span>Already have a account?</span>
                    <Link to={redirect === '/' ? "login": "login?redirect="+ redirect}
                        className="btnn btnn--secondary btnn--full text-center">Go to Login</Link>
                </div>
            </Form>
        </div>
    )
}

export default RegisterScreen
