import React, { useState, useEffect } from 'react'
import { Link , useHistory, useLocation} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import { Form, Button } from 'react-bootstrap'

import authApi from '../api/authApi'
import { loginUser } from '../actions/userAction'

function LoginScreen() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch();
    const history = useHistory()
    const location = useLocation()
    
    const userSignin = useSelector(state=> state.userInfo)
    const redirect = location.search? location.search.split('=')[1] : '/'
    const {user} = userSignin
    useEffect(() => {
        if(user){
            history.push(redirect)
        }
        return () => {
            //
        }
    }, [user])
    const onSubmit = (e) => {
        e.preventDefault()
        const login = async ({ email, password }) => {
            try {
                const response = await authApi.login({ email, password })
                dispatch(loginUser(response))
            } catch (error) {
                console.log("Failed to Login :", error )
            }
        }
        login({ email, password })
    }
    return (
        <div className='form' onSubmit={(e) => onSubmit(e)}>
            <Form className='form__container'>
                <h3 className='form__header'>Login</h3>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email"
                        value={email} name='email'
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                        value={password} name='password'
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <Button className='btnn btnn--primary btnn--full' type="submit">
                    Login
                </Button>
                <div className='form__link'>
                    <span>New to Account?</span>
                    <Link to={redirect === '/' ? "register": "register?redirect="+ redirect}
                        className="btnn btnn--secondary btnn--full text-center">Create a new Account</Link>
                </div>
            </Form>
        </div>
    )
}

export default LoginScreen
