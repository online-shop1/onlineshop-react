import * as types from '../constants'

const productListReducer = (state= {products: [], error: '', loading: false}, action)=>{
    const findProduct = (products, item) =>{
        let index = -1;
        if(products.length > 0){
            for(let i = 0; i< products.length; i++){
                if(products[i]._id === item._id){
                    return index = i;
                }
            }
        }
        return index;
    }
    switch (action.type) {
        case types.FETCH_PRODUCTS_REQUEST:
            return {...state, loading: true}
        case types.FETCH_PRODUCTS_SUCCESS:
            const products = action.payload
            return {...state,
                products,
                loading: false,
                error: ''
            }
        case types.FETCH_PRODUCTS_FAILURE:
            return {...state,
                products: [],
                loading: false,
                error: action.payload
            }
        case types.DELETE_PRODUCT:{
            const products = [...state.products]
            const productId = action.payload
            const index = findProduct(products,productId )
            if(index !== -1){
                products.splice(index, 1)
            }
            return {...state, products}
        }
        default:
            return state;
    }
}

const productDetailReducer = (state= {product: {}, loading: false, error: ''}, action)=>{
    switch (action.type) {
        // case types.FETCH_DETAIL_PRODUCT:
        //     return {...state, product :action.payload}
        case types.FETCH_DETAIL_PRODUCT_REQUEST:
            return {...state, loading : true}
        case types.FETCH_DETAIL_PRODUCT_SUCCESS:
            return {
                ...state,
                loading: false,
                product :action.payload,
                error: ''
            }
        case types.FETCH_DETAIL_PRODUCT_FAILURE:
            return {
                ...state,
                loading: false,
                product :{},
                error: action.payload
            }
        default:
            return state;
    }
}

const productSaveReducer = (state= {product: {}}, action)=>{
    switch (action.type) {
        case types.SAVE_PRODUCT:
            return {...state, product :action.payload}
        default:
            return state;
    }
}

// const productDeleteReducer = (state= {product: {}}, action)=>{
//     switch (action.type) {
//         case types.DELETE_PRODUCT:
//             return {...state, product :action.payload}
//         default:
//             return state;
//     }
// }
export  {productListReducer,productDetailReducer,productSaveReducer}