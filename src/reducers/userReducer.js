import * as types from '../constants'

const user = JSON.parse(localStorage.getItem('user'))
const initialState = user ? {user} : {}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.USER_LOGIN:
            localStorage.setItem('user', JSON.stringify(action.payload))
            return { ...state, user: action.payload  }
        case types.USER_LOGOUT:
            localStorage.removeItem('user');
            return { ...state, user: null }
        case types.USER_REGISTER:
            console.log(action.payload);
            localStorage.setItem('user', JSON.stringify(action.payload))
            return { ...state, user: action.payload }
        default:
            return state;
    }
}

export { userReducer }