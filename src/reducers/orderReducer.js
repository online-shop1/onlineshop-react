import * as types from '../constants'


const orderCreateReducer = (state = {}, action) => {
    switch (action.type) {
        case types.PLACE_ORDER:
            return { ...state, order: action.payload }
        default:
            return state;
    }
}

const orderListReducer = (state = { orders: [] }, action) => {
    switch (action.type) {
        case types.FETCH_ORDERS:
            return { ...state, orders: action.payload }
        default: 
            return state;
    }
}

const orderDetailsReducer = (state = {order:{}}, action)=>{
    switch (action.type) {
        case types.FETCH_ORDER_DETAILS:
            return {...state, order: action.payload}  
        default:
            return state;
    }
}

export { orderCreateReducer, orderListReducer,orderDetailsReducer }