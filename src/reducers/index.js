import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import {productListReducer, productDetailReducer, productSaveReducer} from './productReducer'
import cartReducer from './cartReducer'
import { userReducer } from './userReducer';
import { orderCreateReducer,orderListReducer, orderDetailsReducer } from './orderReducer';
import thunk from 'redux-thunk';

const appReducers = combineReducers({
    productList : productListReducer,
    productDetail : productDetailReducer,
    productSave: productSaveReducer,
    userInfo : userReducer,
    cart : cartReducer,
    orderCreate: orderCreateReducer,
    orders: orderListReducer,
    orderDetail : orderDetailsReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    appReducers,
    composeEnhancer(applyMiddleware(thunk))
)

export default store