import * as types from '../constants'

const data = JSON.parse(localStorage.getItem('cartItems'))
const initialState = data ? data : []
const cartReducer = (state= {cartItems: initialState, shipping: {}, payment: {}}, action)=>{
    const findProductInCart = (cart, product) =>{
        let index = -1;
        if(cart.length > 0){
            for(let i = 0; i< cart.length; i++){
                if(cart[i].product._id === product._id){
                    return index = i;
                }
            }
        }
        return index;
    }
    switch (action.type) {
        case types.ADD_TO_CART:
            const data = action.payload;
            const cartItems = [...state.cartItems]
            let index = findProductInCart(cartItems, data.product)
            if(index !== -1){
                cartItems[index].qty = data.qty
            }else{
                cartItems.push(data)
            }
            localStorage.setItem('cartItems' , JSON.stringify(cartItems))
            return {...state,cartItems}
        case types.DELETE_ITEM_IN_CART:{
            const cartItems = [...state.cartItems.filter(x => x.product._id !== action.payload)]
            localStorage.setItem('cartItems' , JSON.stringify(cartItems))
            return {...state,cartItems}}
        case types.REMOVE_ALL_ITEMS:
            localStorage.removeItem('cartItems');
            return {...state,cartItems: []}
        case types.SAVE_SHIPPING:
            return {...state, shipping : action.payload}
        case types.SAVE_PAYMENT:
            return {...state, payment: action.payload}
        default:
            return state;
    }
}
export default cartReducer